﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HouseScript : MonoBehaviour
{

    bool exited;
    bool played;
    bool added;
    bool showed;
    [SerializeField] private GameObject Cthulhu;
    float timer;
    [SerializeField] private AudioSource cthulhuAudio;
    private float insanity;
    [SerializeField] Slider insanitySlider;
    [SerializeField] Light worldLight;
    [SerializeField] GameObject staffs;
    [SerializeField] GameObject totems;


    // Use this for initialization
    void Start()
    {
        exited = false;
        timer = 3.0f;
        played = false;
        added = false;
        showed = false;
        staffs.SetActive(false);
        totems.SetActive(false);
    }

    void Update()
    {
        if (played)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                if (!showed)
                {
                    BlackScreen.Instance.dark = true;
                    showed = true;
                }
                staffs.SetActive(true);
                totems.SetActive(true);
                Cthulhu.SetActive(false);

                if (!added)
                {
                    insanity = insanitySlider.value + Random.Range(15, 21);
                    insanitySlider.value = insanity;
                    added = true;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (exited == false)
        {
            Show(Cthulhu, cthulhuAudio);
            exited = true;
        }

    }

    void Show(GameObject gameObject, AudioSource audioSource)
    {
        gameObject.SetActive(true);
        audioSource.Play();
        played = true;

    }

}
