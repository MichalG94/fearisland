﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Escape : MonoBehaviour {

	// Update is called once per frame
	void Update () {

        if (Input.GetKey("Cancel"))
            SceneManager.LoadScene(0);

	}
}
