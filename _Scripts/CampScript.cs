﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CampScript : MonoBehaviour {

    [SerializeField] GameObject skull;
    [SerializeField] GameObject player;
    [SerializeField] Slider insanitySlider;
    private float insanity;
    private bool entered, appeared;
    GameObject Skull;
    Vector3 skull_pos;
    Quaternion skull_rot;
    float timer;

	// Use this for initialization
	void Start () {
        entered = false;
        timer = Random.Range(5,8);
        skull_pos = transform.position;
        skull_pos.y += 2;
    }
	
	// Update is called once per frame
	void Update () {

        if (entered) timer -= Time.deltaTime;

        if (timer <= 0 && !appeared)
        {
            //blackscreen
            BlackScreen.Instance.dark = true;

            Vector3 relativePos = player.transform.position - transform.position;
            skull_rot = Quaternion.LookRotation(relativePos);
            Skull = Instantiate(skull, skull_pos, skull_rot);
            appeared = true;
            TowerScript.Instance.sounds.clip = TowerScript.Instance.Laugh;
            TowerScript.Instance.sounds.Play();
            insanity = insanitySlider.value + Random.Range(15, 21);
            insanitySlider.value = insanity;
            appeared = true;
        }

	}

    private void OnTriggerEnter(Collider other)
    {
        entered = true;
    }

    IEnumerator Wait(int seconds)
    {
        yield return new WaitForSeconds(seconds);
    }
}
