﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveFood : MonoBehaviour {

    public LayerMask m_PlayerMask;
    public float m_StartingFood = 25f;
    private float m_CurrentFood;
    private float radius = 10f;
    private bool wasVisited;

	// Use this for initialization
	void Start () {
        wasVisited = false;
        m_CurrentFood = m_StartingFood; 
	}

    private void OnTriggerEnter(Collider other)
    {
        if (!wasVisited)
        {
            
            Collider[] colliders = Physics.OverlapSphere(transform.position, radius, m_PlayerMask);

            for (int i = 0; i < colliders.Length; i++)
            {
                Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

                if (!targetRigidbody)
                {
                    continue;
                }

                UnityStandardAssets.Characters.FirstPerson.FirstPersonController targetPerson =
                    targetRigidbody.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();

                if (!targetPerson)
                {
                    continue;
                }

                //targetPerson.GetFood(m_CurrentFood);

                wasVisited = true;

            }
        }
    }

}
