﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PortalScript : MonoBehaviour {

    public Slider insanitySlider;
    public static bool fifty;
    public static bool ninety;
    public Vector3 destination;
    public GameObject player;

    //public GameObject portalLight;
    public Light portalLight;

	// Use this for initialization
	void Start () {

        fifty = false;
        ninety = false;
        portalLight.gameObject.SetActive(false);

        //portalLight.SetActive(false);
        //this.GetComponentInChildren<LineRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (insanitySlider.value >= 50 && insanitySlider.value <90 && !fifty)
        {
            Debug.Log("FIFTY");
            fifty = true;
            portalLight.gameObject.SetActive(true);
            //this.GetComponentInChildren<LineRenderer>().enabled = true;
            //portalLight.SetActive(true);
        }

        if (insanitySlider.value >=90 && !ninety)
        {
            Debug.Log("NINETY");
            ninety = true;
            portalLight.color = Color.blue;

            //portalLight.GetComponent<LineRenderer>().startColor = Color.blue;
        }
		
	}

    private void OnTriggerEnter(Collider other)
    {
        
        if (fifty)
        {
            player.transform.position = destination;
        }

        if (ninety)
        {
            //koniec gry
            SceneManager.LoadScene(3);
        }
    }
}
