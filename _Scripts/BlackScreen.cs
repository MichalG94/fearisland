﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlackScreen : MonoBehaviour {

    public static BlackScreen Instance;
    public Image screen;
    public bool dark;
    private float timer;

    // Use this for initialization
    void Start () {
        Instance = this;
        dark = false;
        timer = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {

        if (dark)
        {
            screen.color = Color.black;
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                screen.color = Color.clear;
                dark = false;
                timer = 1.0f;
            }
        }
		
	}

}
