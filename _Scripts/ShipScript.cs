﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipScript : MonoBehaviour {

    private Vector3 wreck_pos;
    GameObject player;
    GameObject wreckage;
    private bool shipRange;
    [SerializeField] Slider insanitySlider;
    private float insanity;
    bool black;
    bool wrecked;

    // Use this for initialization
    void Start () {

        wreck_pos = TowerScript.wreck_pos;
        shipRange = false;
        player = GameObject.Find("Player");
        wreckage = Resources.Load<GameObject>("Wreck");
        black = false;
        wrecked = false;
		
	}
	
	// Update is called once per frame
	void Update () {

        if (GlobalScript.ship_appeared)
        {

            if (Vector3.Distance(transform.position, player.transform.position) <= 150)
                shipRange = true;

            if (Vector3.Distance(transform.position, wreck_pos) <= 35 && shipRange && !black && !wrecked)
            {
                wrecked = true;
                BlackScreen.Instance.dark = true;

                //dźwięk pioruna i śmiech
                StartCoroutine(WreckageSounds());

                GameObject.Instantiate(wreckage, transform.position, Quaternion.Euler(-18.0f, -120.0f, 7.5f));
                insanity = insanitySlider.value + Random.Range(15, 21);
                insanitySlider.value = insanity;
                Destroy(gameObject);
                black = true;
            }
        }	
	}

    public IEnumerator WreckageSounds()
    {
        TowerScript.Instance.sounds.clip = TowerScript.Instance.Thunder;
        TowerScript.Instance.sounds.Play();

        yield return new WaitForSeconds(3.0f);

        TowerScript.Instance.sounds.clip = TowerScript.Instance.Laugh;
        TowerScript.Instance.sounds.Play();
    }
}
