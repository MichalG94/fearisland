﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScript : MonoBehaviour {

    public AudioClip[] soundtrack;
    private AudioSource ost;

    void Awake()
    {
        ost = GetComponent<AudioSource>();
    }

    void Start()
    {
        PlayRandomMusic();
    }

    void PlayRandomMusic()
    {
        ost.clip = soundtrack[Random.Range(0, soundtrack.Length-1)];
        ost.Play();

        Invoke("PlayRandomMusic", ost.clip.length + 0.5f);
    }

}
