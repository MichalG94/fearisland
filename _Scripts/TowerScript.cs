﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TowerScript : MonoBehaviour {

    public static TowerScript Instance;

    private float timer;
    public static bool entered;
    [SerializeField] GameObject stairs;
    [SerializeField] public GameObject ship;
    [SerializeField] public AudioSource sounds;
    [SerializeField] public AudioClip Laugh, Thunder;
    public static Vector3 wreck_pos;
    GameObject Ship;
    public static bool added;
    private bool ship_created;
    private float speed;
    private float insanity;
    [SerializeField] Slider insanitySlider;

	// Use this for initialization
	void Start () {

        timer = Random.Range(10,21);
        entered = false;
        added = false;
        ship_created = false;
        speed = 50.0f;
        wreck_pos = new Vector3(1399.0f, 49.0f, 1793.0f);
        Instance = this;
    }
	
	// Update is called once per frame
	void Update () {

        if (entered)
        {

            if (!ship_created)
            {
                //tworzymy statek, który zmierza do portu
                Vector3 ship_position = new Vector3(2425.0f, 52.0f, 2400.0f);
                Ship = Instantiate(ship, ship_position, Quaternion.Euler(0, -120, 0));

                ship_created = true;
                GlobalScript.ship_appeared = true;
            }

            if (ship_created)
                if (Vector3.Distance(Ship.transform.position, wreck_pos) >= 30)
                    Ship.transform.Translate(Vector3.forward * Time.deltaTime * speed, Space.Self);

            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                Object.Destroy(stairs);

                if (!added)
                {
                    insanity = insanitySlider.value + Random.Range(20, 26);
                    insanitySlider.value = insanity;
                    sounds.clip = Laugh;
                    sounds.Play();
                    added = true;
                }
                
            }
        }

	}

    private void OnTriggerEnter(Collider other)
    {
        entered = true;
    }
}
