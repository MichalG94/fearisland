﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CemeteryScript : MonoBehaviour {

    private float countdown;
    private bool inRange;
    public GameObject Player;
    public AudioSource soundtrack;
    public AudioSource danse;
    public AudioSource rattling;
    public bool cemeteryEntered;
    public static bool moved;

    // Use this for initialization
    void Start () {
        this.GetComponentInChildren<LineRenderer>().enabled = false;
        countdown = 5.0f;
        inRange = false;
        cemeteryEntered = false;
	}
	
	// Update is called once per frame
	void Update () {

        if(TowerScript.entered == true && !this.GetComponentInChildren<LineRenderer>().enabled)
        {
            this.GetComponentInChildren<LineRenderer>().enabled = true;
        }

        float dist = Vector3.Distance(Player.transform.position, transform.position);

        if (dist <= 100)
        {
            countdown -= Time.deltaTime;
            if (countdown <= 0)
            {
                rattling.Play();
                countdown = 5.0f;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("cemetery entered");
        soundtrack.volume = 0.0f;
        danse.Play();
        danse.volume = 1.0f;
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("cemetery enxited");
        soundtrack.volume = 1.0f;
        danse.Stop();

    }

}
