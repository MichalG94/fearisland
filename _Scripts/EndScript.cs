﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndScript : MonoBehaviour {

    public Image bg;
    int speed;
    float timer;
    bool showed;

	// Use this for initialization
	void Start () {
        speed = 10;
        timer = 3.0f;
        showed = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (bg.color.a > 0 && !showed)
        {
            //pojawianie się napisu
            Color oldA = bg.color;
            oldA.a -= Time.deltaTime/2;
            bg.color = oldA;
        }
        
        if (bg.color.a <= 0)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0) showed = true;

        if (showed && bg.color.a < 1)
        {
            Color oldA = bg.color;
            oldA.a += Time.deltaTime/2;
            bg.color = oldA;
        }

        if (showed && bg.color.a >= 1)
        {
            SceneManager.LoadScene(0);
        }
		
	}
}
