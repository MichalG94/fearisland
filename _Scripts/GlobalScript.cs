﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GlobalScript : MonoBehaviour {

    private float countdown;
    private bool inTower;
    public static bool cemeteryEntered;
    public static bool ship_appeared;
    [SerializeField] public Slider insanitySlider;
    public static float insanity;
    private bool skeletons;

	// Use this for initialization
	void Start () {
        countdown = 5.0f;
        insanitySlider.value = 0;
        cemeteryEntered = false;
        ship_appeared = false;
        skeletons = false;
        insanity = 0.0f;
        insanitySlider.value = 0.0f;
	}

    // update is called once per frame
    void update()
    {
        if (cemeteryEntered)
        {
            insanity = insanitySlider.value + Random.Range(5, 11);
            insanitySlider.value = insanity;
            cemeteryEntered = false;
        }
    }
}
