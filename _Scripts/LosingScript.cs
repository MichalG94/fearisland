﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LosingScript : MonoBehaviour {

    public Slider insanitySlider;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if (insanitySlider.value >= 100)
        {
            SceneManager.LoadScene(4);
        }

	}
}
