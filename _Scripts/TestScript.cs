﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestScript : MonoBehaviour {

    public Sprite[] photos;
    public Sprite blckScr;
    public Image screen;
    float timer;
    int photoIndex;
    bool black;
    bool photo;

	// Use this for initialization
	void Start () {
        photoIndex = 0;
        screen.sprite = blckScr;
        black = true;
        photo = false;
        timer = 2.0f;
	}
	
	// Update is called once per frame
	void Update () {

        Debug.Log(photoIndex);

        timer -= Time.deltaTime;

        if (timer <= 0 && black)
        {
            black = false;
            screen.sprite = photos[photoIndex];
            timer = 10.0f;
            photo = true;
        }

        if (timer <= 0 && photo)
        {
            photo = false;
            photoIndex++;
            if (photoIndex < photos.Length)
            {
                screen.sprite = blckScr;
                timer = 2.0f;
                black = true;
            }
            else
            {
                SceneManager.LoadScene(0);
            }

        }

        		
	}

}
